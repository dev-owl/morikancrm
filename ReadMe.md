# READ ME

本書では開発システムの概要、Git運用ルール、Gitのリポジトリ構成を定義する

## Quick summary

案件管理～顧客管理までをクラウド上で行うシステム。  
kintoneを活用して案件管理～顧客管理までを実施する。

## Git operational rules

##### 【開発作業の流れ】

masterブランチからdevelopブランチを作成  
developブランチから実装する機能毎にfeatureブランチを作成  
featureブランチで実装完了した機能はdevelopブランチにマージ  
リリース作業開始時点で、developからreleaseブランチを作成  
リリース作業完了時点で、releaseからdevelop, masterブランチにマージ  

##### 【リリース後の障害対応の流れ】
masterブランチからhotfixブランチを作成  
hotfixブランチで障害対応が完了した時点で、develop, masterブランチにマージ  

##### 【登場するブランチ】

| **ブランチ名** | **派生元** | **概要**                                                     |
| :------------- | :--------- | :----------------------------------------------------------- |
| master         | －         | リリースした時点のソースコードを管理するブランチ。 客先にリリースしたタイミングではTagを切って提供する。 |
| develop        | master     | 開発作業の主軸となるブランチ。次回リリースまでの期間はこのブランチで管理。 |
| release        | develop    | developでの開発作業完了後、リリース時の微調整を行うブランチ。 　(バージョン番号やデザイン微調整、難読化などで利用する) |
| hotfix         | master     | リリースされた製品に致命的なバグ(クラッシュなど)があった場合に緊急対応をするためのブランチ。 |
| feature        | develop    | 実装する機能毎or個人毎のブランチ。  (feature/◯◯, feature/xxなど) |

※詳細はConfluence参照のこと
https://dev-owl.atlassian.net/wiki/spaces/knowledge/pages/10158101/Bitbucket

##### 【gitコミットコメントルール】

gitのコミットコメントは下記のフォーマットとする。
スペースや改行も含めてルールとします。

```
<Prefix>: <Subject>

<Body>

<Footer>
```

コミットメッセージは4つの項目からなります。

1. Prefix 何をしたかを接頭辞で短くあらわします
2. Subject 何をしたかを短い文章にします
3. Body なぜそれをしたのかを文章にします
4. Footer 補足情報を載せます

```
＜記載例＞
Update: npmのパッケージをすべて最新版に更新する 

1年前に公開後、更新されていなかった。 
改修が始まるため、使用するパッケージやライブラリも新しいバージョンを使うようにする 

#test
```
※詳細はConfluence参照のこと
https://dev-owl.atlassian.net/wiki/spaces/knowledge/pages/14483457/git

## Structure

root  
├─1_Design                                       ：設計関係の資料  
│  ├─10_img                                              ：仕様書・設計書の画像ファイル  
│  ├─11_FunctionalSpecification.md    ：機能仕様書  
│  ├─12_ArchitectureDesign.md           ：アーキテクチャ設計書  
│  └─13_ScreenDesign.md                     ：詳細設計書  
├─2_Program                                    ：プログラム本体  
│  ├─kinPlugin                                         ：kintonePlguinソース（Plugin毎にフォルダ作成）  
│  │  ├─PluginXXXX1                                    ：kintonePlguin一式  
│  │  │  ├─src                                                    ：kintonePlguinソースコード  
│  │  │  │   ├─css                                                    ：kintonePlguin cssファイル群  
│  │  │  │   ├─html                                                 ：kintonePlguin htmlファイル群  
│  │  │  │   ├─img                                                   ：kintonePlguin 画像ファイル群  
│  │  │  │   ├─js                                                       ：kintonePlguin jsファイル群  
│  │  │  │   └─manifest.json                                  ：kintonePlguin マニフェストファイル  
│  │  │  └─XXX.ppk                                           ：kintonePluginパッケージ化key  
│  │  │  └─kintonePluginCmd.bat                 ：kintonePluginパッケージ化のbat  
│  │  └─PluginXXXX2  
│  │       └─src  
│  └─nodejsApp                                      ：Node.js用ソースコード一式（構成検討中）  
│      └─src  
├─3_Test                                            ：テスト関係の資料  
│  ├─30_TestingEnvironment               ：テスト環境  
│  ├─31_TestSpecification                     ：テスト仕様書  
│  ├─32_TestResult                                 ：テスト結果  
│  └─33_TestSummary                          ：テストまとめ  
├─4_Release                                      ：リリース資料  
│  ├─40_OperationalEnvironment      ：運用環境  
│  ├─41_BuildEnvironment                  ：ビルド環境  
│  ├─42_ReleaseModule                       ：リリースモジュール一式  
│  └─43_Manual                                     ：操作マニュアル  
├─5_Operation                                  ：運用関係の資料  
│  └─51_OperationManual                   ：運用マニュアル  
├─6_Other                                        ：その他資料  
│  └─61_minutes                                    ：客先提出議事録  
│       │    └─img                                            ：客先提出議事録用の画像ファイル  
│       ├─yy-mm-dd_XXXXXXXX.md            ：客先提出議事録（日付毎にファイル作成）  
│       └─yy-mm-dd_templete.md        ：客先提出議事録のテンプレート  
└─ReadMe.md                                 ：本書  